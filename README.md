# OpenIAM 4.2.0.X Highly Available Cluster

## Requirements
The article describes the process to install OpenIAM version 4.2.0.X in the HA mode. There are base Linux Operation system administration knowledge is required. **We are working as a root user! (if you are not you will need a sudo access)**
### Hardware Requirements
Three servers (VM) with the following configuration each:
 - HDD (prefer SSD) - more than 40gb
 - RAM - 48GB
 - CPU (vCPU) - more than 4 cores.


### Before start

- Switch over to the root account.
```
sudo -i
```

- Install git on one node which will be master in term of installation procedure.
```
dnf install git -y
```

- Generate ssh keys pair on the master. (press enter on each question you will be asked)

```bash
ssh-keygen
```
- Propagate master public key to the all cluster nodes.

On the master node:

```bash
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
cat ~/.ssh/id_rsa.pub
```

Copy the value you have got from the above command to the buffer.

On the other nodes:

ssh to the other nodes and issue the command there 

```bash
echo "Paste here the value from the buffer" >> ~/.ssh/authorized_keys
```
Example:

```bash
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKcniMIHk8xwrR5lMQjCGS1fQSqJoOv1vStYORTW8ni7HS4diPtAEFbHMe/uYyIowMzwo6nBI7bzxyzwAPwSiuxQ7Qrv/FdNChS2sBT2pQVMHow81YA31XN6gutGQNVBMMCGe2FeVgPB8+GKvWIM9hxHCCVIfg/s/xNV7HFGJhd+mVIQZuN2OfMbfmJ1l11IhsKhgoF6OY6AzEOuesIY+sBnEIaRbUT12tBrqKIPwLgyeLH5fnp31sBmY637nCjc4pN4tQsiTdaxDuR4Q5Ynp2iF37NFuOBvCAOKfSDGX8gcN/uxqI24se5BpGSISizWLE+sFUstHgQFdbKsCtgMsv node1" >> ~/.ssh/authorized_keys
```

- Clone git repository [openiam-cluster-rpm](https://bitbucket.org/openiam/openiam-cluster-rpm/src/main/). It contains utilities to simplify installation process.
You may need to install your ssh key on bitbucket. [Here is](https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/) how to do it.
```
git clone git@bitbucket.org:openiam/openiam-cluster-rpm.git
cd openiam-cluster-rpm
```

## Installation

 1. Update file infra.conf according to your infrastructure (use vi or nano util to replace default values of the node`s names and IP addresses )
 
 ```bash
 nano ./infra.conf
 ```

 2. Run file install.sh. Your Internet access must be available.

 ```bash
 ./install.sh
 ```
 Wait while it will finish, it could take a long time.

 3. Answer the quetions you will be asked during initialization: 


## Start OpenIAM
The cluster OpenIAM components should start automatically after initialization. However, there is required sometimes to 
start/stop cluster

To **start** OpenIAM components at single node please run: 
```bash
openiam-cli start
```

To **stop** OpenIAM components at single node please run:
```bash
openiam-cli stop
```

To **start** OpenIAM components at cluster nodes please run: 
```bash
cluster openiam-cli start
```

To **stop** OpenIAM components at cluster nodes please run:
```bash
cluster openiam-cli stop
```
