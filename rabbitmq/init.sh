#!/usr/bin/env bash
echo "Install RABBITMQ"

. ../infra.conf

cluster /usr/local/openiam/utils/rabbitmq/init.sh

cluster systemctl stop rabbitmq-server

sleep 10

scp /var/lib/rabbitmq/.erlang.cookie ${CLUSTER_NODES[2]}:/var/lib/rabbitmq/.erlang.cookie
scp /var/lib/rabbitmq/.erlang.cookie ${CLUSTER_NODES[3]}:/var/lib/rabbitmq/.erlang.cookie

cluster systemctl start rabbitmq-server

sleep 10

ssh ${CLUSTER_NODES[2]} rabbitmqctl stop_app
ssh ${CLUSTER_NODES[2]} rabbitmqctl join_cluster rabbit@${CLUSTER_NODES[1]}
ssh ${CLUSTER_NODES[2]} rabbitmqctl start_app
ssh ${CLUSTER_NODES[3]} rabbitmqctl stop_app
ssh ${CLUSTER_NODES[3]} rabbitmqctl join_cluster rabbit@${CLUSTER_NODES[1]}
ssh ${CLUSTER_NODES[3]} rabbitmqctl start_app

rabbitmqctl set_policy -p  openiam_am  ha-all ".*" '{"ha-mode":"all"}'
rabbitmqctl set_policy -p  openiam_idm  ha-all ".*" '{"ha-mode":"all"}'
rabbitmqctl set_policy -p  openiam_audit  ha-all ".*" '{"ha-mode":"all"}'
rabbitmqctl set_policy -p  openiam_common  ha-all ".*" '{"ha-mode":"all"}'
rabbitmqctl set_policy -p  openiam_connector  ha-all ".*" '{"ha-mode":"all"}'
rabbitmqctl set_policy -p  openiam_activiti  ha-all ".*" '{"ha-mode":"all"}'
rabbitmqctl set_policy -p  openiam_user  ha-all ".*" '{"ha-mode":"all"}'
rabbitmqctl set_policy -p  openiam_groovy_manager  ha-all ".*" '{"ha-mode":"all"}'
rabbitmqctl set_policy -p  openiam_synchronization  ha-all ".*" '{"ha-mode":"all"}'
rabbitmqctl set_policy -p  openiam_ext_log  ha-all ".*" '{"ha-mode":"all"}'
rabbitmqctl set_policy -p  openiam_bulk_synchronization  ha-all ".*" '{"ha-mode":"all"}'
rabbitmqctl set_policy -p  openiam_reconciliation  ha-all ".*" '{"ha-mode":"all"}'
rabbitmqctl set_policy -p  openiam_bulk_reconciliation  ha-all ".*" '{"ha-mode":"all"}'

m4 -DNODE_NAME="localhost" rabbitmq.properties.m4 > rabbitmq.properties
cp rabbitmq.properties /usr/local/openiam/conf/properties/rabbitmq.properties
