spring.rabbitmq.host=NODE_NAME
spring.rabbitmq.port=5672
org.openiam.rabbitmq.hosts=${spring.rabbitmq.host}:${spring.rabbitmq.port}

org.openiam.rabbitmq.concurrent.consumers=20
org.openiam.rabbitmq.max.concurrent.consumers=50
org.openiam.rabbitmq.prefetch.count=2

org.openiam.rabbitmq.channelTransacted=true
org.openiam.rabbitmq.channelCacheSize=10

org.openiam.mq.broker.encryption.key=ff808181670838e0016708610547001b

rabbitmq.ssl.key-store=file://${confpath}/conf/rabbitmq/client/rabbitmq.jks
