#!/usr/bin/env bash
. /usr/local/openiam/env.conf

cd /usr/src
rpm2cpio openiam-4.2.X.noarch.x86_64.rpm | cpio -idmv
tar -xvzf /usr/src/tmp/openiam-tmproot/openiam.tar.gz

STATUS="$(systemctl is-active mariadb.service)"
if [ "${STATUS}" = "active" ]; then
   FLYWAY_DATABASE_TYPE="mysql"
  if [ ! -d "$HOME_DIR/conf/schema/mysql" ]; then
    mkdir -p $HOME_DIR/conf/schema/mysql
  fi
   cp -rf /usr/src/openiam/conf/schema/mysql/openiam $HOME_DIR/conf/schema/mysql/
   cp -rf /usr/src/openiam/conf/schema/mysql/activiti $HOME_DIR/conf/schema/mysql/
   cp -rf /usr/src/openiam/conf/properties/datasource.properties $HOME_DIR/conf/properties/
   sed -i 's/localhost/master/g' $HOME_DIR/conf/properties/datasource.properties
   db_host_url="jdbc:mysql://${FLYWAY_OPENIAM_HOST}:${FLYWAY_OPENIAM_PORT}"
   openiam_jdbc_url="jdbc:mysql://${FLYWAY_OPENIAM_HOST}:${FLYWAY_OPENIAM_PORT}/${FLYWAY_OPENIAM_DATABASE_NAME}?autoReconnect=true&useUnicode=true&characterEncoding=utf8&connectionCollation=utf8_general_ci&serverTimezone=UTC"
   activiti_jdbc_url="jdbc:mysql://${FLYWAY_ACTIVITI_HOST}:${FLYWAY_ACTIVITI_PORT}/${FLYWAY_ACTIVITI_DATABASE_NAME}?autoReconnect=true&useUnicode=true&characterEncoding=utf8&connectionCollation=utf8_general_ci&serverTimezone=UTC"
fi

cd -


cp -f ./install.sh /usr/local/openiam/utils/flyway/init.sh
chown openiam:openiam /usr/local/openiam/utils/flyway/init.sh
chmod 755 /usr/local/openiam/utils/flyway/init.sh
/usr/local/openiam/utils/flyway/init.sh

chown openiam:openiam /usr/local/openiam/conf/properties/datasource.properties
