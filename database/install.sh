#!/bin/bash

set -e

. /usr/local/openiam/env.conf
export VAULT_CERTS="$HOME_DIR/vault/certs/"
export VAULT_HOME="$HOME_DIR/utils/vault/"
export FLYWAY="$HOME_DIR/flyway/flyway"
export FLYWAY_UTIL_HOME="$HOME_DIR/utils/flyway/"
. $HOME_DIR/utils/vault/validate.vault.sh

function random() {
  echo $(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
}

export FLYWAY_BASELINE_VERSION="0.0.0.0"

function input() {
  echo -e "$3, default: $2"
  read value
  if [ -z "$value" ]; then
    value=$2
  fi
  export $1=$value
}

# $1 name of var, $2 is default val, $3 is message to show.
function input_pass() {
  echo -e "$3, default: $2"
  read -s value
  if [ -z "$value" ]; then
    value=$2
  fi
  export $1=$value
}
echo -e "Database access information"

function db_type() {
  input "FLYWAY_DATABASE_TYPE" "mysql" "Possible values: mysql, postgres, mssql, oracle. Type of the database that you are going to use with OpenIAM. The RDBMS have to be already installed"
  case $FLYWAY_DATABASE_TYPE in
  mysql)
    prompt_db
    ;;
  postgres)
    prompt_db
    ;;
  mssql)
    prompt_db
    ;;
  oracle)
    echo "!! YOU SELECTED ORACLE. You have to create openiam and activiti users and schemas manually!!!"
    input FLYWAY_ORACLE_SID "" "Oracle SID, leave empty if you use oracle service name"
    input FLYWAY_ORACLE_SERVICE_NAME "" "Oracle Service name, leave empty if you SID"
    ;;
  *)
    echo "please type one of  mysql, postgres, mssql, oracle"
    db_type
    ;;
  esac
}
export IS_INIT="N"
export FLYWAY_OPENIAM_USERNAME=$(. ${VAULT_HOME}vault.fetch.property.sh vault.secret.jdbc.username)
export FLYWAY_OPENIAM_PASSWORD=$(. ${VAULT_HOME}vault.fetch.property.sh vault.secret.jdbc.password)
export FLYWAY_ACTIVITI_USERNAME=$(. ${VAULT_HOME}vault.fetch.property.sh vault.secret.activiti.jdbc.username)
export FLYWAY_ACTIVITI_PASSWORD=$(. ${VAULT_HOME}vault.fetch.property.sh vault.secret.activiti.jdbc.password)

prompt_db() {
  read -p "Do you want to initialize OpenIAM Schema and Users? Select this if you are not created schema and users in RDBMS yet. Super user (root) password will required [y/n]:" is_init
  case "$is_init" in
  [yY][eE][sS] | [yY])
    echo "Initialization".
    input "SU_NAME" "root" "Enter username for Super user (for mysql this is root)"
    input "SU_PASSWORD" "" "Enter password for super user (sa or root, depend on the db type)"
    m4 -DJDBC_OPENIAM_DB_USER="${FLYWAY_OPENIAM_USERNAME}" \
      -DJDBC_OPENIAM_DB_PASSWORD="${FLYWAY_OPENIAM_PASSWORD}" \
      -DJDBC_ACTIVITI_DB_USER="${FLYWAY_ACTIVITI_USERNAME}" \
      -DJDBC_ACTIVITI_DB_PASSWORD="${FLYWAY_ACTIVITI_PASSWORD}" \
      -DOPENIAM_DATABASE_NAME="${FLYWAY_OPENIAM_DATABASE_NAME}" \
      -DACTIVITI_DATABASE_NAME="${FLYWAY_ACTIVITI_DATABASE_NAME}" \
      ${FLYWAY_UTIL_HOME}/V0.0.0.0.000__initialization.sql.${FLYWAY_DATABASE_TYPE}.m4 >${FLYWAY_UTIL_HOME}/V0.0.0.0.000__initialization.sql
    export IS_INIT="Y"

    ;;
  [nN][oO] | [nN])
    echo "skip database Initialization installation"
    FLYWAY_BASELINE_VERSION="2.3.0.0"
    ;;
  *)
    echo "Please answer [y/n]"
    prompt_db
    ;;
  esac
}

function prompt_collect() {
  echo "Please validate information below"
  echo "---------------------------------"
  echo "FLYWAY_BASELINE_VERSION=${FLYWAY_BASELINE_VERSION}"
  echo "FLYWAY_OPENIAM_DATABASE_NAME=${FLYWAY_OPENIAM_DATABASE_NAME}"
  echo "FLYWAY_ACTIVITI_DATABASE_NAME=${FLYWAY_ACTIVITI_DATABASE_NAME}"
  echo "FLYWAY_OPENIAM_HOST=${FLYWAY_OPENIAM_HOST}"
  echo "FLYWAY_OPENIAM_PORT=${FLYWAY_OPENIAM_PORT}"
  echo "FLYWAY_ACTIVITI_HOST=${FLYWAY_ACTIVITI_HOST}"
  echo "FLYWAY_ACTIVITI_PORT=${FLYWAY_ACTIVITI_PORT}"
  echo "FLYWAY_DATABASE_TYPE=${FLYWAY_DATABASE_TYPE}"
  if [ "oracle" == "${FLYWAY_DATABASE_TYPE}" ]; then
    echo "FLYWAY_ORACLE_SID=${FLYWAY_ORACLE_SID}"
    echo "FLYWAY_ORACLE_SERVICE_NAME=${FLYWAY_ORACLE_SERVICE_NAME}"
  fi
  echo "Database will be initialized=${IS_INIT}"
  if [ "Y" == "${IS_INIT}" ]; then
    echo "Root (Db admin) user name=${SU_NAME}"
    echo "Root (Db admin) user password=${SU_PASSWORD}"
  fi
   echo "---------------------------------"
  read -p "Please validate your input above, if your are OK with that enter 'y'. To repeat an information collecting procedure enter 'n' :" is_agree
  case "$is_agree" in
  [yY][eE][sS] | [yY])
    return 0
    ;;
  [nN][oO] | [nN])
    collect
    ;;
  *)
    echo "Please answer [y/n]"
    prompt_collect
    ;;
  esac
}

function collect() {
  echo -e "=============== CRITICAL SECTION ==============="
  echo -e "Database configuration."
  input "FLYWAY_BASELINE_VERSION" "$FLYWAY_BASELINE_VERSION" "Use default value if this is new installation. If you are doing update, specify your current (before update) version here, like 4.1.11.0"
  input "FLYWAY_OPENIAM_DATABASE_NAME" "$FLYWAY_OPENIAM_DATABASE_NAME" "This is the name of the openiam core database.  If using mariadb, this is most likely 'openiam'"
  input "FLYWAY_ACTIVITI_DATABASE_NAME" "$FLYWAY_ACTIVITI_DATABASE_NAME" "This is the name of the openiam Activiti database.  If using mariadb, this is most likely 'activiti'"

  db_type

  input "FLYWAY_OPENIAM_HOST" "$FLYWAY_OPENIAM_HOST" "This is the hostname of where the openiam core database is."
  input "FLYWAY_OPENIAM_PORT" "$FLYWAY_OPENIAM_PORT" "This is the port of where the openiam core database is. If using mariadb, this is most likely '3306'"

  input "FLYWAY_ACTIVITI_HOST" "$FLYWAY_ACTIVITI_HOST" "This is the hostname of where the openiam activiti database is."
  input "FLYWAY_ACTIVITI_PORT" "$FLYWAY_ACTIVITI_PORT" "This is the port of where the openiam activiti database is. If using mariadb, this is most likely '3306'"
  prompt_collect
}

collect

openiam_jdbc_url=''
activiti_jdbc_url=''
db_host_url=''

case $FLYWAY_DATABASE_TYPE in
mysql)

  db_host_url="jdbc:mysql://${FLYWAY_OPENIAM_HOST}:${FLYWAY_OPENIAM_PORT}"
  openiam_jdbc_url="jdbc:mysql://${FLYWAY_OPENIAM_HOST}:${FLYWAY_OPENIAM_PORT}/${FLYWAY_OPENIAM_DATABASE_NAME}?autoReconnect=true&useUnicode=true&characterEncoding=utf8&connectionCollation=utf8_general_ci&serverTimezone=UTC"
  activiti_jdbc_url="jdbc:mysql://${FLYWAY_ACTIVITI_HOST}:${FLYWAY_ACTIVITI_PORT}/${FLYWAY_ACTIVITI_DATABASE_NAME}?autoReconnect=true&useUnicode=true&characterEncoding=utf8&connectionCollation=utf8_general_ci&serverTimezone=UTC"
  ;;
postgres)
  db_host_url="jdbc:postgresql://${FLYWAY_OPENIAM_HOST}:${FLYWAY_OPENIAM_PORT}"
  openiam_jdbc_url="jdbc:postgresql://${FLYWAY_OPENIAM_HOST}:${FLYWAY_OPENIAM_PORT}/${FLYWAY_OPENIAM_DATABASE_NAME}?useUnicode=true&characterEncoding=UTF-8"
  activiti_jdbc_url="jdbc:postgresql://${FLYWAY_ACTIVITI_HOST}:${FLYWAY_ACTIVITI_PORT}/${FLYWAY_ACTIVITI_DATABASE_NAME}?useUnicode=true&characterEncoding=UTF-8"
  ;;
mssql)
  db_host_url="jdbc:sqlserver://${FLYWAY_OPENIAM_HOST}:${FLYWAY_OPENIAM_PORT}"
  openiam_jdbc_url="jdbc:sqlserver://${FLYWAY_OPENIAM_HOST}:${FLYWAY_OPENIAM_PORT};databaseName=${FLYWAY_OPENIAM_DATABASE_NAME};integratedSecurity=false;encrypt=false;trustServerCertificate=true"
  activiti_jdbc_url="jdbc:sqlserver://${FLYWAY_ACTIVITI_HOST}:${FLYWAY_ACTIVITI_PORT};databaseName=${FLYWAY_ACTIVITI_DATABASE_NAME};integratedSecurity=false;encrypt=false;trustServerCertificate=true"
  ;;
oracle)
  if [ ! -z "$FLYWAY_ORACLE_SID" ]; then
    openiam_jdbc_url="jdbc:oracle:thin:@${FLYWAY_OPENIAM_HOST}:${FLYWAY_OPENIAM_PORT}:${FLYWAY_ORACLE_SID}"
    activiti_jdbc_url="jdbc:oracle:thin:@${FLYWAY_ACTIVITI_HOST}:${FLYWAY_ACTIVITI_PORT}:${FLYWAY_ORACLE_SID}"
    cp ${FLYWAY_UTIL_HOME}oracle.sid.properties.m4 ${FLYWAY_UTIL_HOME}$FLYWAY_DATABASE_TYPE.properties.m4
  elif [ ! -z "$FLYWAY_ORACLE_SERVICE_NAME" ]; then
    openiam_jdbc_url="jdbc:oracle:thin:@${FLYWAY_OPENIAM_HOST}:${FLYWAY_OPENIAM_PORT}/${FLYWAY_ORACLE_SERVICE_NAME}"
    activiti_jdbc_url="jdbc:oracle:thin:@${FLYWAY_ACTIVITI_HOST}:${FLYWAY_ACTIVITI_PORT}/${FLYWAY_ORACLE_SERVICE_NAME}"
    cp ${FLYWAY_UTIL_HOME}oracle.service.properties.m4 ${FLYWAY_UTIL_HOME}$FLYWAY_DATABASE_TYPE.properties.m4
  else
    echo "either FLYWAY_ORACLE_SID or FLYWAY_ORACLE_SERVICE_NAME must be specified as an environment variable when using '$FLYWAY_DATABASE_TYPE' as the backend database"
    exit 1
  fi
  ;;
*)
  exit 0
  ;;
esac

cp ${FLYWAY_UTIL_HOME}$FLYWAY_DATABASE_TYPE.properties.m4 ${FLYWAY_UTIL_HOME}datasource.properties.m4
m4 -DJDBC_HOST="$FLYWAY_OPENIAM_HOST" \
        -DJDBC_PORT="$FLYWAY_OPENIAM_PORT" \
        -DJDBC_SCHEMA_NAME="$FLYWAY_OPENIAM_DATABASE_NAME" \
        -DJDBC_SID="$FLYWAY_ORACLE_SID" \
        -DJDBC_USERNAME="" \
        -DJDBC_PASSWORD="" \
        -DJDBC_DATABASE_NAME="$FLYWAY_OPENIAM_DATABASE_NAME" \
        -DJDBC_ACTIVITI_DATABASE_NAME="$FLYWAY_ACTIVITI_DATABASE_NAME" \
        -DJDBC_ACTIVITI_SCHEMA_NAME="$FLYWAY_ACTIVITI_DATABASE_NAME" \
        -DJDBC_ACTIVITI_USERNAME="" \
        -DJDBC_ACTIVITI_PASSWORD="" \
        -DOPENIAM_HIBERNATE_DIALECT="" \
        -DJDBC_INCLUDE_SCHEMA_IN_QUERIES="false" \
        -DHIBERNATE_INCLUDE_SCHEMA_IN_QUERIES="false" \
        -DJDBC_SERVER_TIMEZONE="UTC" \
        ${FLYWAY_UTIL_HOME}datasource.properties.m4 > $HOME_DIR/conf/properties/datasource.properties

if [ "$IS_INIT" == "Y" ]; then
  if [ "$FLYWAY_DATABASE_TYPE" == "mysql" ]; then
    echo "Mysql. Try to initialize automatically"
    mysql -u${SU_NAME} -h${FLYWAY_OPENIAM_HOST} -p${SU_PASSWORD} < ${FLYWAY_UTIL_HOME}/V0.0.0.0.000__initialization.sql
  else
    cp ${FLYWAY_UTIL_HOME}/V0.0.0.0.000__initialization.sql /tmp/init.sql
    echo "Please perform script located at /tmp/init.sql to initialize Database MANUALLY. File contains secrets! Remove it after initialization."
    read -r -s -p $'Press enter to continue database initialization'
  fi
  export FLYWAY_BASELINE_VERSION="2.3.0.0"
fi

echo "${HOME_DIR}/conf/schema/${FLYWAY_DATABASE_TYPE}/openiam/"

$FLYWAY -url="${openiam_jdbc_url}" \
  -user=${FLYWAY_OPENIAM_USERNAME} \
  -password=${FLYWAY_OPENIAM_PASSWORD} \
  -baselineVersion=$FLYWAY_BASELINE_VERSION baseline \
  -locations="filesystem:${HOME_DIR}/conf/schema/${FLYWAY_DATABASE_TYPE}/openiam/" \
  -mixed=true \
  -placeholderReplacement=false migrate

$FLYWAY -url="${activiti_jdbc_url}" \
  -user=${FLYWAY_ACTIVITI_USERNAME} \
  -password=${FLYWAY_ACTIVITI_PASSWORD} \
  -baselineVersion=$FLYWAY_BASELINE_VERSION baseline \
  -locations="filesystem:${HOME_DIR}/conf/schema/${FLYWAY_DATABASE_TYPE}/activiti" \
  -mixed=true \
  -placeholderReplacement=false migrate

echo "Done"
