#!/bin/bash

. /usr/local/openiam/env.conf
export VAULT_CERTS="$HOME_DIR/vault/certs/"
#. $HOME_DIR/utils/vault/validate.vault.sh
token=$(curl -k --request POST --cert ${VAULT_CERTS}/vault.crt --key ${VAULT_CERTS}/vault.key --data '{"name": "web"}' https://${VAULT_URL}:${VAULT_PORT}/v1/auth/cert/login | jq .auth.client_token  | tr -d '"')

key=$1
value=""
curl -k \
    --header "X-Vault-Token: $token" \
    --request POST \
    --cert ${VAULT_CERTS}/vault.crt --key ${VAULT_CERTS}/vault.key \
    --data '{"value": "'$value'"}' \
    https://${VAULT_URL}:${VAULT_PORT}/v1/secret/openiam/$key
