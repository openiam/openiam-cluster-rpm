sentinelServersConfig:
  idleConnectionTimeout: 10000
  connectTimeout: 10000
  timeout: 5000
  retryAttempts: 3
  retryInterval: 1500
  failedSlaveReconnectionInterval: 3000
  failedSlaveCheckInterval: 60000
  subscriptionsPerConnection: 5
  clientName: null
  loadBalancer: !<org.redisson.connection.balancer.RoundRobinLoadBalancer> {}
  subscriptionConnectionMinimumIdleSize: 1
  subscriptionConnectionPoolSize: 50
  slaveConnectionMinimumIdleSize: 24
  slaveConnectionPoolSize: 64
  masterConnectionMinimumIdleSize: 24
  masterConnectionPoolSize: 64
  readMode: "SLAVE"
  subscriptionMode: "SLAVE"
  sentinelAddresses:
  - "redis://HAPROXY_REDIS:26379"
  masterName: "mymaster"
  keepAlive: true
  database: 0
  checkSentinelsList: false
eventLoopGroup: null
threads: 16
nettyThreads: 32
codec: !<org.redisson.codec.SnappyCodec> {}
transportMode: "NIO"

#https://github.com/redisson/redisson/issues/4356
addressResolverGroupFactory: !<org.openiam.redis.OpeniamDnsAddressResolverGroupFactory> {}
