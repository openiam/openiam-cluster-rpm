#!/usr/bin/env bash

. /usr/local/openiam/env.conf

yum install $HOME_DIR/openiamrepo/redis-6.2.7-1.el8.remi.x86_64.rpm -y

cp -f $HOME_DIR/utils/redis/redis.conf /etc/redis/redis.conf
cp -f $HOME_DIR/utils/redis/sentinel.conf /etc/redis/sentinel.conf

sysctl vm.overcommit_memory=1

chmod 664 /etc/redis/sentinel.conf
chmod 664 /etc/redis/redis.conf

systemctl enable redis
systemctl start redis
systemctl daemon-reload

systemctl enable --now sentinel

echo "Starting Redis server..."
sleep 10
