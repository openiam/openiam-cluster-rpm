#!/usr/bin/env bash

. /usr/local/openiam/env.conf
. ../infra.conf

export VAULT_CERTS="$HOME_DIR/vault/certs/"
export JAVA_HOME="$HOME_DIR/jdk"
export VAULT_HOME=$HOME_DIR/utils/vault/

REDIS_PASSWORD=$(. ${VAULT_HOME}vault.fetch.property.sh vault.secret.redis.password)

cp initialization.sh /usr/local/openiam/utils/redis/init.sh
scp initialization.sh ${CLUSTER_NODES[2]}:/usr/local/openiam/utils/redis/init.sh
scp initialization.sh ${CLUSTER_NODES[3]}:/usr/local/openiam/utils/redis/init.sh

# Configure redis master+slave
m4 -DNODE_IP="${CLUSTER_IPS[1]}" -DREDIS_PASS=$REDIS_PASSWORD redis-master.conf.m4 > redis.conf.${CLUSTER_NODES[1]}
m4 -DNODE_IP="${CLUSTER_IPS[2]}" -DREDIS_PASS=$REDIS_PASSWORD redis-slave.conf.m4 > redis.conf.${CLUSTER_NODES[2]}
m4 -DNODE_IP="${CLUSTER_IPS[3]}" -DREDIS_PASS=$REDIS_PASSWORD redis-slave.conf.m4 > redis.conf.${CLUSTER_NODES[3]}

cp redis.conf.${CLUSTER_NODES[1]}  /usr/local/openiam/utils/redis/redis.conf
scp redis.conf.${CLUSTER_NODES[2]} ${CLUSTER_NODES[2]}:/usr/local/openiam/utils/redis/redis.conf
scp redis.conf.${CLUSTER_NODES[3]} ${CLUSTER_NODES[3]}:/usr/local/openiam/utils/redis/redis.conf

# Configure sentinels
m4 -DREDIS_PASS=$REDIS_PASSWORD -DNODE_IP="${CLUSTER_IPS[1]}" sentinel.conf.m4 > sentinel.conf.${CLUSTER_NODES[1]}
m4 -DREDIS_PASS=$REDIS_PASSWORD -DNODE_IP="${CLUSTER_IPS[2]}" sentinel.conf.m4 > sentinel.conf.${CLUSTER_NODES[2]}
m4 -DREDIS_PASS=$REDIS_PASSWORD -DNODE_IP="${CLUSTER_IPS[3]}" sentinel.conf.m4 > sentinel.conf.${CLUSTER_NODES[3]}

cp sentinel.conf.${CLUSTER_NODES[1]} /usr/local/openiam/utils/redis/sentinel.conf
scp sentinel.conf.${CLUSTER_NODES[2]} ${CLUSTER_NODES[2]}:/usr/local/openiam/utils/redis/sentinel.conf
scp sentinel.conf.${CLUSTER_NODES[3]} ${CLUSTER_NODES[3]}:/usr/local/openiam/utils/redis/sentinel.conf


m4 -DHAPROXY_REDIS="${CLUSTER_IPS[1]}" redis.properties.m4 > redis.properties
cp redis.properties /usr/local/openiam/conf/properties/
chown openiam:openiam /usr/local/openiam/conf/properties/redis.properties

cp sentinel.service /etc/systemd/system/
scp sentinel.service ${CLUSTER_NODES[2]}:/etc/systemd/system/
scp sentinel.service ${CLUSTER_NODES[3]}:/etc/systemd/system/

cluster /usr/local/openiam/utils/redis/init.sh

sleep 5

m4 -DHAPROXY_REDIS="${CLUSTER_NODES[1]}" ./redisson.yaml.m4 > ./redisson.yaml
cp redisson.yaml /usr/local/openiam/conf/properties/
