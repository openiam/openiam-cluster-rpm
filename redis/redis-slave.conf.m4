bind NODE_IP
protected-mode no
port 6379
save ""
rename-command FLUSHDB ""
rename-command FLUSHALL ""
replica-announce-port 6390
replica-announce-ip master
tcp-backlog 511
timeout 0
tcp-keepalive 300
daemonize no
pidfile "/var/run/redis_6379.pid"
loglevel notice
logfile "/var/log/redis/redis.log"
dbfilename "dump.rdb"
dir "/var/lib/redis"
replica-serve-stale-data yes
replica-read-only yes
requirepass "REDIS_PASS"
masterauth "REDIS_PASS"
replicaof master 6379
