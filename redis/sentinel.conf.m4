
bind NODE_IP
port 26379

sentinel deny-scripts-reconfig yes
sentinel monitor mymaster master 6379 2
sentinel down-after-milliseconds mymaster 2000
sentinel auth-pass mymaster REDIS_PASS

requirepass "REDIS_PASS"
#protected-mode no

logfile "/var/log/redis/sentinel.log"
pidfile "/var/run/redis/sentinel.pid"

daemonize yes
sentinel announce-hostnames yes
sentinel resolve-hostnames yes
sentinel announce-port 26379
sentinel announce-ip "master"
