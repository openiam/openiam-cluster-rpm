# ============================================================================ #
#                H A P r o x y  -  H a s h i c o r p   V a u l t
# ============================================================================ #

frontend vault
    mode tcp
    description "Vault"
    bind *:VAULT_PORT
    option tcplog
    redirect scheme https code 301 if !{ ssl_fc }
    default_backend vault

backend vault
    mode tcp
    description "Vault"
    balance first
    option httpchk GET /v1/sys/health
    http-check expect string '"standby":false'
    server vault1 NODE_1:8200 check check-ssl verify none
    server vault2 NODE_2:8200 check check-ssl verify none
    server vault3 NODE_3:8200 check check-ssl verify none
