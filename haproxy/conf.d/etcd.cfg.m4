#---------------------------------------------------------------------
# round robin balancing between the etcd backends
#---------------------------------------------------------------------

frontend etcd
  bind *:2310
  default_backend etcd_backend

backend etcd_backend
  balance roundrobin
  option httpchk GET /health
  http-check expect string '"health": "true"'
  server etcd1 NODE_1:2379 check
  server etcd2 NODE_2:2379 check
  server etcd3 NODE_3:2379 check
