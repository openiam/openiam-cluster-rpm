# ============================================================================ #
#                H A P r o x y  -  Openiam ESB
# ============================================================================ #

frontend esb
    bind *:ESB_PORT
    default_backend openiam-esb

backend openiam-esb
    default-server check  observe layer7  error-limit 10  on-error mark-down  inter 1s  rise 10  slowstart 20s
    balance     roundrobin
    option redispatch
    retries 3
    option httpchk GET /openiam-esb/actuator/health
#    http-check expect rstring (\{\"status\":\"UP\"\})
    http-check expect rstring .*(\{\"status\":\"UP\"\}).*
    server openiam-esb-1 NODE_1:9080 check
    server openiam-esb-2 NODE_2:9080 check
    server openiam-esb-3 NODE_3:9080 check 

