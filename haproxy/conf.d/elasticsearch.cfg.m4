#---------------------------------------------------------------------
# round robin balancing between the elasticsearch backends
#---------------------------------------------------------------------
frontend elasticsearch
    mode http
    bind *:ES_PORT
    default_backend elasticsearch

backend elasticsearch
    balance     roundrobin
    server elasticsearch-01 NODE_1:9200 check
    server elasticsearch-02 NODE_2:9200 check
    server elasticsearch-03 NODE_3:9200 check 
