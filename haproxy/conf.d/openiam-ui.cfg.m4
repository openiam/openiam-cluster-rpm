#---------------------------------------------------------------------
# round robin balancing between the openiam-ui backends
#---------------------------------------------------------------------

frontend ui
    bind *:UI_PORT
    default_backend openiam-ui

backend openiam-ui
    default-server check  observe layer7  error-limit 10  on-error mark-down  inter 1s  rise 10  slowstart 20s
    balance        roundrobin
    option redispatch
    retries 3
    option httpchk GET /idp/actuator/health
    http-check expect rstring (\{\"status\":\"UP\"\})
    server openiam-ui-01 NODE_1:8080 check
    server openiam-ui-02 NODE_2:8080 check
    server openiam-ui-03 NODE_3:8080 check

