#---------------------------------------------------------------------
# round robin balancing between the openiam-rabbitmq-ui backends
#---------------------------------------------------------------------
frontend rabbitmq-ui
    bind *:RMQ_UI_PORT
    default_backend openiam-rabbitmq-ui

backend openiam-rabbitmq-ui
    balance     roundrobin
    server openiam-rabbitmqui-01 NODE_1:15672 check
    server openiam-rabbitmqui-02 NODE_2:15672 check
    server openiam-rabbitmqui-03 NODE_3:15672 check  

