#---------------------------------------------------------------------
# round robin balancing between the redis cluster nodes
#---------------------------------------------------------------------
frontend redis
    mode tcp
    bind *:6382 name redis
    default_backend redis-cluster

backend redis-cluster
    mode tcp
    option tcpka
    option tcp-check
    tcp-check send PING\r\n
    tcp-check expect string +PONG
    tcp-check send info\ replication\r\n
    tcp-check expect string role:master
    tcp-check send QUIT\r\n
    tcp-check expect string +OK
    server redis-01-7100 NODE_1_IP:7100 check inter 10s
    server redis-01-7101 NODE_1_IP:7101 check inter 10s
    server redis-01-7102 NODE_1_IP:7102 check inter 10s
    server redis-02-7100 NODE_2_IP:7100 check inter 10s
    server redis-02-7101 NODE_2_IP:7101 check inter 10s
    server redis-02-7102 NODE_2_IP:7102 check inter 10s
    server redis-03-7100 NODE_3_IP:7100 check inter 10s
    server redis-03-7101 NODE_3_IP:7101 check inter 10s
    server redis-03-7102 NODE_3_IP:7102 check inter 10s

defaults REDIS
mode tcp
timeout connect 3s
timeout server 6s
timeout client 6s

frontend frontend_redis
    bind *:6378 name redis
    default_backend backend_redis

backend backend_redis
    option tcp-check
    tcp-check connect
    tcp-check send AUTH\ REDIS_PASS\r\n
    tcp-check send PING\r\n
    tcp-check expect string +PONG
    tcp-check send info\ replication\r\n
    tcp-check expect string role:master
    tcp-check send QUIT\r\n
    tcp-check expect string +OK
    server redis_1 NODE_1:6379 check inter 1s
    server redis_2 NODE_2:6379 check inter 1s 
    server redis_3 NODE_3:6379 check inter 1s
