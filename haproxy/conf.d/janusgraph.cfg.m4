#---------------------------------------------------------------------
# round robin balancing between the janusgrapg backends
#---------------------------------------------------------------------
frontend janusgraph
    bind *:JANUS_PORT
    default_backend janusgraph

backend janusgraph
    balance     roundrobin
    server janusgraph-01 NODE_1:8182 check
    server janusgraph-02 NODE_2:8182 check
    server janusgraph-03 NODE_3:8182 check 

