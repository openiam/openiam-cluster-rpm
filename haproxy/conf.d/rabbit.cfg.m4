frontend rabbit
    mode  tcp
    bind *:RMQ_PORT
    default_backend rabbit-cluster

#---------------------------------------------------------------------
# round robin balancing between rabbitmq backends
#---------------------------------------------------------------------
backend rabbit-cluster
    mode	tcp
#    option 	tcp-check
    balance     roundrobin
    server	rabbit-01	NODE_1:5672	check
    server	rabbit-02	NODE_2:5672	check
    server	rabbit-03	NODE_3:5672	check
