#!/bin/bash

. ../infra.conf
. /usr/local/openiam/env.conf
export VAULT_CERTS="$HOME_DIR/vault/certs/"
export VAULT_HOME="$HOME_DIR/utils/vault/"


prompt_proxy() {
read -p "Do you want to install OpenIAM reverse proxy module? [y/n]:" is_proxy
case "$is_proxy" in
    [yY][eE][sS]|[yY])
        echo "Installing reverse proxy module"

        cluster yum install httpd mod_ssl mod_openiam -y 
        
        cluster setsebool httpd_can_network_connect=1
	    m4 -DUI_PORT="${HAPROXY_UI_PORT}" -DESB_PORT="$HAPROXY_ESB_PORT" $INST_HOME/proxy/mod_openiam.conf.m4 > $INST_HOME/proxy/mod_openiam.conf
        
        cp $INST_HOME/proxy/mod_openiam.conf  /etc/httpd/conf.d/mod_openiam.conf
        scp $INST_HOME/proxy/mod_openiam.conf ${CLUSTER_NODES[2]}:/etc/httpd/conf.d/mod_openiam.conf
        scp $INST_HOME/proxy/mod_openiam.conf ${CLUSTER_NODES[3]}:/etc/httpd/conf.d/mod_openiam.conf

        cluster firewall-cmd --add-port=80/tcp --permanent
	    cluster firewall-cmd --reload
        cluster systemctl enable httpd
        cluster systemctl start httpd
        ;;
    [nN][oO]|[nN])
        echo "Skip Proxy module installation"
        ;;
    *)
        echo "Please answer [y/n]"
        prompt_proxy
        ;;
esac
}

prompt_proxy
