####################################################
# OpenIAM apache module example configurations
#

# Loading OpenIAM apache module
LoadModule openiam_module         modules/mod_openiam.so


# Turn Off Proxy Requests. Only Reverse Proxying will be allowed
ProxyRequests off


<ifModule ssl_module>
    # Enable SSL Proxying just in case it will be used for reverse-proxying
    # Even if you don't use https, this config option allow to do proxying from http to https backends
    SSLProxyEngine on
</ifModule>


# OpenIAM ESB path

OPENIAM_ESBPath       http://localhost:ESB_PORT

<VirtualHost *:80>

ErrorLog logs/mod_openiam_error_log
TransferLog logs/mod_openiam_access_log

# Debug options. Turned off by default.

LogLevel warn
#OPENIAM_Verbose on
#OPENIAM_DebugPatterns on
#OPENIAM_DebugCookies on
#OPENIAM_DebugESB on
#OPENIAM_DebugCertAuth on
#OPENIAM_DumpRequests on
#OPENIAM_DumpRequestsBody  on
#OPENIAM_DumpResponses     on
#OPENIAM_AllowDumpHeaders  on
#OPENIAM_DumpNoAuth        off

KeepAlive on

<Location />
    AuthType openiam
    AuthName "OpenIAM"
    Require valid-openiam-federation
    Require openiam-configure

    OPENIAM_DefaultUrl /selfservice/

    OPENIAM_CSPEnabled on
    OPENIAM_CORSAllowAll on

    #OPENIAM_SoapAuthHeaders "//*[local-name()='authentication'][1]"
</Location>


# Begin Websockets support


RewriteEngine on
RewriteCond %{HTTP:Upgrade} websocket [NC]
RewriteRule /(.*) ws://localhost:UI_PORT/$1 [P,L]

<Location /idp/openiam-socket/info>
    AuthType openiam
    AuthName "OpenIAM"
    Require valid-openiam-federation

    OPENIAM_CSPEnabled off
    OPENIAM_CORSAllowAll on
</Location>

<Location /idp/openiam-socket/iframe.html>
    AuthType openiam
    AuthName "OpenIAM"
    Require valid-openiam-federation

    OPENIAM_CSPEnabled off
    OPENIAM_CORSAllowAll on
</Location>

# End Websockets support


<Location /static>
    AuthType none
    AuthName "none"

    OPENIAM_CSPEnabled on
    OPENIAM_CORSAllowAll off
</Location>

<Location /server-status>
    SetHandler server-status
    AuthType none
    AuthName "none"
    Require local
</Location>

SetEnvIf Request_URI "^/server-status$" dontlog

RewriteEngine On
RewriteRule ^/webconsole$      /webconsole/      [NC,R=302,L]
RewriteRule ^/selfservice$     /selfservice/     [NC,R=302,L]
RewriteRule ^/selfservice-ext$ /selfservice-ext/ [NC,R=302,L]

ErrorDocument 401 /openiam-ui-static/401
ErrorDocument 404 /openiam-ui-static/404

#Configure Host

OPENIAM_ConfigureHost /webconsole/setup
OPENIAM_ConfigureUrls /webconsole/setup
OPENIAM_ConfigureUrls /webconsole/setup/metadata
OPENIAM_ConfigureUrls /webconsole/rest/api/ui/public/bootstrap/metadata
OPENIAM_ConfigureUrls /webconsole/setup/contentprovider
OPENIAM_ConfigureUrls /webconsole/challengeResponse
OPENIAM_ConfigureUrls /server-status



OPENIAM_ConfigureBackend http://localhost:UI_PORT

OPENIAM_DefaultUrl /selfservice/

######## COMPRESSION CONFIG OPTIONS

<ifModule mod_deflate.c>
    AddOutputFilterByType DEFLATE text/html
    AddOutputFilterByType DEFLATE text/plain
    AddOutputFilterByType DEFLATE text/xml application/xhtml+xml application/xml
    AddOutputFilterByType DEFLATE text/css
    AddOutputFilterByType DEFLATE text/javascript application/javascript application/x-javascript
    AddOutputFilterByType DEFLATE application/json
    AddOutputFilterByType DEFLATE application/rss+xml

    DeflateCompressionLevel 6

    <ifModule mod_setenvif.c>
        #Do not compress following file types
        SetEnvIfNoCase Request_URI \.(?:exe|dll|so)$                                        no-gzip dont-vary
        SetEnvIfNoCase Request_URI \.(?:iso|bin|raw)$                                       no-gzip dont-vary
        SetEnvIfNoCase Request_URI \.(?:sit)$                                               no-gzip dont-vary
        SetEnvIfNoCase Request_URI \.(?:t?gz|zip|tar|bz2|rar)$                              no-gzip dont-vary
        SetEnvIfNoCase Request_URI \.(?:png|jpe?g|gif|tif?f)$                               no-gzip dont-vary
        SetEnvIfNoCase Request_URI \.(?:flv|swf|mp3)$                                       no-gzip dont-vary
        SetEnvIfNoCase Request_URI \.(?:3gp|mp3|aa|aac|flac|m4a|ogg|voc|wav|wma|webm)$      no-gzip dont-vary
        SetEnvIfNoCase Request_URI \.(?:avi|mov|mkv|vob|ogv|gifv|mng|m?ts|qt|wmv|adf|amv)$  no-gzip dont-vary
        SetEnvIfNoCase Request_URI \.(?:mp4|m4p|mpe?g|mp2|mpe|mpv|m4v)$                     no-gzip dont-vary
        SetEnvIfNoCase Request_URI \.(?:woff2?|svg|ttf|otf|eot)$                            no-gzip dont-vary

        BrowserMatch ^Mozilla/4 gzip-only-text/html
        BrowserMatch ^Mozilla/4\.0[678] no-gzip
        BrowserMatch \bMSIE !no-gzip !gzip-only-text/html
    </ifModule>

</ifModule>

######## COMPRESSION CONFIG OPTIONS END

</VirtualHost>
