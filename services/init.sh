#!/bin/bash
. /usr/local/openiam/env.conf
. ../infra.conf

cluster echo "1" \> /usr/local/openiam/initialized
cluster chmod -R 777 /usr/local/openiam/logs
cluster echo "green" \> ${HOME_DIR}/infra.status
cluster systemctl enable openiam.service

cluster firewall-cmd --add-port={9080/tcp,${HAPROXY_ESB_PORT}/tcp} --permanent
cluster firewall-cmd --add-port=8080/tcp --permanent
cluster firewall-cmd --reload

m4 -DESB_PORT="${HAPROXY_ESB_PORT}" ./service-urls.properties.m4 > ./service-urls.properties
cp -f service-urls.properties ${HOME_DIR}/conf/properties/

chown -R openiam:openiam /usr/local/openiam/conf/

for i in 1 2 3; do
    echo "Run Openiam on "${CLUSTER_NODES[$i]}"..."
    ssh ${CLUSTER_IPS[$i]} systemctl enable --now openiam-auth.service
    ssh ${CLUSTER_IPS[$i]} systemctl enable --now openiam-device.service
    ssh ${CLUSTER_IPS[$i]} systemctl enable --now openiam-email.service
    ssh ${CLUSTER_IPS[$i]} systemctl enable --now openiam-esb.service
    ssh ${CLUSTER_IPS[$i]} systemctl enable --now openiam-groovy.service
    ssh ${CLUSTER_IPS[$i]} systemctl enable --now openiam-idm.service
    ssh ${CLUSTER_IPS[$i]} systemctl enable --now openiam-reconciliation.service
    ssh ${CLUSTER_IPS[$i]} systemctl enable --now openiam-synchronization.service
    ssh ${CLUSTER_IPS[$i]} systemctl enable --now openiam-ui.service
    ssh ${CLUSTER_IPS[$i]} systemctl enable --now openiam-workflow.service
    ssh ${CLUSTER_IPS[$i]} systemctl enable --now openiam-business-rule.service
    ssh ${CLUSTER_IPS[$i]} systemctl enable --now openiam-ui.service
    sleep 120
  done

############ Uncomment below 2 lines to enable module for use SafeNet Authentication API
#$HOME_DIR/utils/sas/init.sh
#systemctl enable --now openiam-sas.service

#echo "update httpd to $HTTPD_VERSION..."
#sleep 5
#update_httpd
#httpd -v
#sleep 5
#openiam-cli status

