openiam.esb.host=localhost
openiam.esb.port=ESB_PORT
openiam.service_base=${openiam.esb.host}\:${openiam.esb.port}
webservice.path=servlet\://
openiam.service_host=http://${openiam.service_base}/
openiam.idm.ws.path=openiam-esb/idmsrvc/
openiam.connector.ws.path=openiam-esb/idmsrvc/
