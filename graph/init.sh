#!/bin/bash

. /usr/local/openiam/env.conf
. ../infra.conf

export VAULT_CERTS="$HOME_DIR/vault/certs/"
export JAVA_HOME="$HOME_DIR/jdk"
export VAULT_HOME=$HOME_DIR/utils/vault/


function check_cassandra() {
  echo $1
  if (( $1 > $2  )); then
    return 1
  fi
  cluster nodetool -h ::FFFF:127.0.0.1 status
  if [[ "0" == $? ]]; then
   echo "Cassandra alive"
   return 0
  else
    echo "Waiting for cassandra"
    sleep 20
    let "a = $1 + 1"
    check_cassandra $a $2
  fi
}

cluster dnf install cassandra -y

m4 -DSEED_IP="${CLUSTER_IPS[1]}" -DNODE_IP="${CLUSTER_IPS[1]}" ./cassandra.yaml.m4 > ./${CLUSTER_NODES[1]}_cassandra.yaml
m4 -DSEED_IP="${CLUSTER_IPS[1]}" -DNODE_IP="${CLUSTER_IPS[2]}" ./cassandra.yaml.m4 > ./${CLUSTER_NODES[2]}_cassandra.yaml
m4 -DSEED_IP="${CLUSTER_IPS[1]}" -DNODE_IP="${CLUSTER_IPS[3]}" ./cassandra.yaml.m4 > ./${CLUSTER_NODES[3]}_cassandra.yaml

cluster cp -rf /etc/cassandra/default.conf/cassandra.yaml /etc/cassandra/default.conf/cassandra.yaml.original

for i in 1 2 3; do
  #scp ./cassandra.yaml ${CLUSTER_IPS[$i]}:/etc/cassandra/default.conf/cassandra.yaml
  scp ${CLUSTER_NODES[$i]}_cassandra.yaml ${CLUSTER_IPS[$i]}:/etc/cassandra/default.conf/cassandra.yaml
done

cluster systemctl enable cassandra.service
sleep 5

#cluster systemctl start cassandra.service
# Run Cassandra on all nodes one by one over timeout to give it time to be autojoined to thhe cluster
systemctl start cassandra.service
sleep 30
ssh ${CLUSTER_IPS[2]} systemctl start cassandra.service
sleep 30
ssh ${CLUSTER_IPS[3]} systemctl start cassandra.service

sleep 30
check_cassandra 0 30

if [[ "0" == $? ]]; then
   echo "Cassandra is ready to use. Continue..."
   else
   echo "Cassandra is Dead. Exit!"
   exit 1
fi

HOSTS=""
echo "Configure janusgraph-cql.properties..."

for i in 1 2 3; do
  HOSTS+=${CLUSTER_NODES[$i]}"," 
done

HOSTS=${HOSTS::-1}
echo $HOSTS


m4 -DHOST_1="${CLUSTER_NODES[1]}" -DHOST_2="${CLUSTER_NODES[2]}" -DHOST_3="${CLUSTER_NODES[3]}" ./janusgraph-cql.properties.m4 > ./janusgraph-cql.properties

for i in 1 2 3; do
  #scp ./cassandra.yaml ${CLUSTER_IPS[$i]}:/etc/cassandra/default.conf/cassandra.yaml
  scp janusgraph-cql.properties ${CLUSTER_IPS[$i]}:/usr/local/openiam/janusgraph/conf/janusgraph-cql.properties
  scp gremlin-server.yaml ${CLUSTER_IPS[$i]}:/usr/local/openiam/janusgraph/conf/gremlin-server/gremlin-server.yaml
done


#cluster sed -i "s/storage.hostname=127.0.0.1/storage.hostname=$HOSTS/g" $HOME_DIR/janusgraph/conf/janusgraph-cql.properties

cluster systemctl start janusgraph.service
cluster systemctl enable janusgraph.service

m4 -DJANUS_PORT="${HAPROXY_JANUS_PORT}" ./gremlin.properties.m4 > ./gremlin.properties
cp ./gremlin.properties $HOME_DIR/conf/properties/
