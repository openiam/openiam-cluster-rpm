output "connection" {
  value = ["ssh -i ${local_file.ssh_key.filename} centos@${aws_instance.oiam_lab.0.public_ip}",
           "ssh -i ${local_file.ssh_key.filename} centos@${aws_instance.oiam_lab.1.public_ip}",
           "ssh -i ${local_file.ssh_key.filename} centos@${aws_instance.oiam_lab.2.public_ip}",
           "ssh -i ${local_file.ssh_key.filename} centos@${aws_instance.mssql.public_ip}",]
}

output "infra_conf" {
  value = <<EOF
export INST_HOME="/root/openiam-cluster-rpm"
export FS="nfs"

export OPENIAM_VERSION="4.2.1.5"

export NFS_SERVER="${aws_instance.oiam_lab.0.private_ip}"

export NETWORK="172.16.0.0/24"
export KEEPALIVE_VIRTUAL_ADDRESS="172.16.0.164"

export KEEPALIVE_MASTER="${aws_instance.oiam_lab.0.private_dns}"

export GALERA_CLUSTER_ADDRESS="gcomm://${aws_instance.oiam_lab.0.private_dns},${aws_instance.oiam_lab.1.private_dns},${aws_instance.oiam_lab.2.private_dns}"

declare -A CLUSTER_NODES=( [1]=${aws_instance.oiam_lab.0.private_dns} [2]=${aws_instance.oiam_lab.1.private_dns} [3]=${aws_instance.oiam_lab.2.private_dns} )
declare -A CLUSTER_IPS=( [1]=${aws_instance.oiam_lab.0.private_ip} [2]=${aws_instance.oiam_lab.1.private_ip} [3]=${aws_instance.oiam_lab.2.private_ip} )

export ES_CLUSTER_HOSTS='["${aws_instance.oiam_lab.0.private_dns}", "${aws_instance.oiam_lab.1.private_dns}", "${aws_instance.oiam_lab.2.private_dns}"]'

export HAPROXY_VAULT_PORT=8203
export HAPROXY_JANUS_PORT=8183
export HAPROXY_ESB_PORT=9081
export HAPROXY_UI_PORT=8085
export HAPROXY_ES_PORT=9201
export HAPROXY_RABBIT_PORT=5673
export HAPROXY_RABBIT_UI_PORT=5002
EOF
}

output "sed" {
  value = ["sed -i 's/.us-west-2.compute.internal//g' infra.conf"]
}
