# OpenIAM 4.2.0.X Highly Available Cluster
This TF script creates 3 AWS EC2 instances for testing HA RPM CLuster 3 nodes installation
and 1 instance with MSSql-server-2019

## Requirements
 - AWS account
 - export AWS_SECRET_ACCESS_KEY=
 - export AWS_ACCESS_KEY_ID=
 - terraform


## Installation

 1. init
 
 ```bash
 terraform init
 ```

 2. apply

 ```bash
 terraform apply
 ```
 
 3. Follow README of the root


## Outputs

The outputs of the script are usefull to connect to the created AWS instances and replace default infra.conf with values the TF retrieves.
