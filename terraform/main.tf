resource "tls_private_key" "pk" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "kp" {
  key_name   = "tfkey"       
  public_key = tls_private_key.pk.public_key_openssh
}

resource "local_file" "ssh_key" {
  filename = "${aws_key_pair.kp.key_name}.pem"
  content  = tls_private_key.pk.private_key_pem
  provisioner "local-exec" { 
    command = "chmod 400 ./${aws_key_pair.kp.key_name}.pem"
  }
}

# Create 3 ec2 instances (Centos8)
resource "aws_instance" "oiam_lab" {
  count         = 3
  ami           = "ami-05c0173ad7f443a17"
  #ami           = "ami-0c1e08ef0564b2e0d"    RHEL8.7
  instance_type = "r5.2xlarge"
  subnet_id     = "subnet-0f25700b176e10978"
  key_name      = aws_key_pair.kp.key_name

  root_block_device {
    volume_size           = "100"
    volume_type           = "gp2"
    encrypted             = false
    delete_on_termination = true
  }
  tags = {
    Name = "node${count.index}"
  }
  provisioner "remote-exec" {
    
    inline = [
    "sudo dnf install -y git",
    "sudo ssh-keygen -t rsa -f /root/.ssh/id_rsa -q -P ''",
    ]
    
    connection {
    type = "ssh"
    user = "centos"
    private_key = file("./${local_file.ssh_key.filename}")
    host = self.public_ip
    }
  }
}

resource "aws_instance" "mssql" {
  ami           = "ami-05c0173ad7f443a17"
  instance_type = "t2.medium"
  subnet_id     = "subnet-0f25700b176e10978"
  key_name      = aws_key_pair.kp.key_name

  root_block_device {
    volume_size           = "20"
    volume_type           = "gp2"
    encrypted             = false
    delete_on_termination = true
  }
  tags = {
    Name = "mssql"
  }
  provisioner "remote-exec" {
    
    inline = [
    "sudo yum install -y yum-utils",
    "sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo",
    "sudo yum install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin",
    "sudo systemctl start docker",
    "sudo docker run -e 'ACCEPT_EULA=Y' -e 'MSSQL_SA_PASSWORD=${var.sa_password}' -p 1433:1433 --name sql1 --hostname sql1 -d mcr.microsoft.com/mssql/server:2019-latest",
    ]
    
    connection {
    type = "ssh"
    user = "centos"
    private_key = file("./${local_file.ssh_key.filename}")
    host = self.public_ip
    }
  }
}
