cluster.name: openiam
cluster.initial_master_nodes: ["NODE_1_NAME", "NODE_2_NAME", "NODE_3_NAME"]
node.name: NODE_NAME
network.host: 0.0.0.0
network.publish_host: NODE_NAME
path.data: /var/lib/elasticsearch
path.logs: /var/log/elasticsearch
discovery.zen.ping.unicast.hosts: ["NODE_1_NAME", "NODE_2_NAME", "NODE_3_NAME"]
discovery.zen.minimum_master_nodes: 2
node.master: true
node.data: true
xpack.security.enabled: true
xpack.security.transport.ssl.enabled: true
xpack.license.self_generated.type: basic
xpack.security.transport.ssl.verification_mode: certificate
xpack.security.transport.ssl.keystore.path: /etc/elasticsearch/certs/elastic-certificates.p12
xpack.security.transport.ssl.truststore.path: /etc/elasticsearch/certs/elastic-certificates.p12
