#!/usr/bin/env bash

echo "Install ELASTICSEARCH"

. ../infra.conf

m4 -DNODE_NAME="${CLUSTER_NODES[1]}" -DNODE_1_NAME="${CLUSTER_NODES[1]}" -DNODE_2_NAME="${CLUSTER_NODES[2]}" -DNODE_3_NAME="${CLUSTER_NODES[3]}" ./elasticsearch.yml.m4 > ./${CLUSTER_NODES[1]}_elasticsearch.yml
m4 -DNODE_NAME="${CLUSTER_NODES[2]}" -DNODE_1_NAME="${CLUSTER_NODES[1]}" -DNODE_2_NAME="${CLUSTER_NODES[2]}" -DNODE_3_NAME="${CLUSTER_NODES[3]}" ./elasticsearch.yml.m4 > ./${CLUSTER_NODES[2]}_elasticsearch.yml
m4 -DNODE_NAME="${CLUSTER_NODES[3]}" -DNODE_1_NAME="${CLUSTER_NODES[1]}" -DNODE_2_NAME="${CLUSTER_NODES[2]}" -DNODE_3_NAME="${CLUSTER_NODES[3]}" ./elasticsearch.yml.m4 > ./${CLUSTER_NODES[3]}_elasticsearch.yml

cluster dnf install -y java-11-openjdk

cluster /usr/local/openiam/utils/elasticsearch/init.sh

# Configure certificates
cluster mkdir -v /etc/elasticsearch/certs
cd /usr/share/elasticsearch/
mkdir -v certs
ES_CERT_PASSWORD=`echo $(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)`

bin/elasticsearch-certutil ca --out certs/elastic-stack-ca.p12 --pass "$ES_CERT_PASSWORD"
bin/elasticsearch-certutil cert --ca certs/elastic-stack-ca.p12 --ca-pass "$ES_CERT_PASSWORD" --out certs/elastic-certificates.p12 --pass "$ES_CERT_PASSWORD"

cp /usr/share/elasticsearch/certs/*.p12 /etc/elasticsearch/certs/
scp /usr/share/elasticsearch/certs/*.p12 ${CLUSTER_NODES[2]}:/etc/elasticsearch/certs/
scp /usr/share/elasticsearch/certs/*.p12 ${CLUSTER_NODES[3]}:/etc/elasticsearch/certs/

echo $ES_CERT_PASSWORD | /usr/share/elasticsearch/bin/elasticsearch-keystore add xpack.security.transport.ssl.keystore.secure_password -xf
echo $ES_CERT_PASSWORD | /usr/share/elasticsearch/bin/elasticsearch-keystore add xpack.security.transport.ssl.truststore.secure_password -xf

ssh ${CLUSTER_NODES[2]} "echo $ES_CERT_PASSWORD | /usr/share/elasticsearch/bin/elasticsearch-keystore add xpack.security.transport.ssl.keystore.secure_password -xf"
ssh ${CLUSTER_NODES[2]} "echo $ES_CERT_PASSWORD | /usr/share/elasticsearch/bin/elasticsearch-keystore add xpack.security.transport.ssl.truststore.secure_password -xf"
ssh ${CLUSTER_NODES[3]} "echo $ES_CERT_PASSWORD | /usr/share/elasticsearch/bin/elasticsearch-keystore add xpack.security.transport.ssl.keystore.secure_password -xf"
ssh ${CLUSTER_NODES[3]} "echo $ES_CERT_PASSWORD | /usr/share/elasticsearch/bin/elasticsearch-keystore add xpack.security.transport.ssl.truststore.secure_password -xf"

cd -

for i in 1 2 3; do
  scp ./${CLUSTER_NODES[$i]}_elasticsearch.yml ${CLUSTER_IPS[$i]}:/etc/elasticsearch/elasticsearch.yml
done

cluster systemctl stop elasticsearch
cluster rm -rf /var/lib/elasticsearch/nodes
cluster chown elasticsearch /etc/elasticsearch/certs/*

echo "Start elasticsearch on ${CLUSTER_NODES[1]}"
systemctl start elasticsearch&
echo "Start elasticsearch on ${CLUSTER_NODES[2]}"
ssh ${CLUSTER_NODES[2]} systemctl start elasticsearch&
echo "Start elasticsearch on ${CLUSTER_NODES[3]}"
ssh ${CLUSTER_NODES[3]} systemctl start elasticsearch& 

echo "Waiting for elasticsearch cluster is ready..."
sleep 30;

ES_VAULT_PASSWORD=$(/usr/local/openiam/utils/vault/vault.fetch.property.sh vault.secret.elasticsearch.password)
ES_CURRENT_PASSWORD=$(/usr/share/elasticsearch/bin/elasticsearch-keystore show keystore.seed)
curl -u "elastic:${ES_CURRENT_PASSWORD}" -XPOST -H "Content-Type: application/json" http://${CLUSTER_IPS[1]}:9200/_security/user/elastic/_password -d "{ \"password\": \"${ES_VAULT_PASSWORD}\" }"

curl -X GET  -u "elastic:${ES_VAULT_PASSWORD}" http://${CLUSTER_IPS[1]}:9200/_cat/nodes
