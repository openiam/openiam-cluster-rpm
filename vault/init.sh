#!/bin/bash
echo "Install VAULT"
. ../infra.conf

chmod +x ./initialization.sh

m4 -DVAULT_PORT="${HAPROXY_VAULT_PORT}" ./vault.properties.m4 > /usr/local/openiam/conf/properties/vault.properties

for i in 1 2 3; do
scp ./initialization.sh ${CLUSTER_IPS[$i]}:/tmp/initialization.sh
done

cluster /tmp/initialization.sh

#only on 1st
/usr/local/openiam/utils/vault/generate.cert.sh

scp /usr/local/openiam/jdk/lib/security/cacerts ${CLUSTER_NODES[2]}:/usr/local/openiam/jdk/lib/security/cacerts
scp /usr/local/openiam/jdk/lib/security/cacerts ${CLUSTER_NODES[3]}:/usr/local/openiam/jdk/lib/security/cacerts

m4 -DNODE_1_IP="${CLUSTER_IPS[1]}" -DNODE_2_IP="${CLUSTER_IPS[2]}"  -DNODE_3_IP="${CLUSTER_IPS[3]}" ./openiam.policy.hcl.m4 > /usr/local/openiam/vault/openiam.policy.hcl

chown vault:vault /usr/local/openiam/vault/openiam.policy.hcl

scp /usr/local/openiam/vault/openiam.policy.hcl ${CLUSTER_NODES[2]}:/usr/local/openiam/vault/openiam.policy.hcl
scp /usr/local/openiam/vault/openiam.policy.hcl ${CLUSTER_NODES[3]}:/usr/local/openiam/vault/openiam.policy.hcl
ssh ${CLUSTER_NODES[2]} chown vault:vault /usr/local/openiam/vault/openiam.policy.hcl
ssh ${CLUSTER_NODES[3]} chown vault:vault /usr/local/openiam/vault/openiam.policy.hcl

chmod +x ./boot.sh
cp ./boot.sh /usr/local/openiam/utils/vault/
scp ./boot.sh ${CLUSTER_NODES[2]}:/usr/local/openiam/utils/vault/
scp ./boot.sh ${CLUSTER_NODES[3]}:/usr/local/openiam/utils/vault/

cluster /usr/local/openiam/utils/vault/boot.sh
#only on 1st node
/usr/local/openiam/utils/vault/bootstrap.sh
#permissions
chmod 777 /usr/local/openiam/vault/certs/vault.jks
