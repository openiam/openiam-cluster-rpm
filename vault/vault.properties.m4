vault.uri=https://localhost:VAULT_PORT
vault.authentication=CERT
vault.ssl.key-store=file:///usr/local/openiam/vault/certs/vault.jks
vault.ssl.key-store-password=changeit
vault.ssl.key-store-type=JKS
