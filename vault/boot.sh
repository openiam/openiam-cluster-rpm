#!/bin/bash
#### THIS IS VAULT INITIALIZATION SCRIPT
. /usr/local/openiam/env.conf
export VAULT_CERTS="$HOME_DIR/vault/certs/"
export JAVA_HOME="$HOME_DIR/jdk"

systemctl enable openiam-vault
systemctl start openiam-vault
sleep 10

$HOME_DIR/utils/vault/start.sh
sleep 5
