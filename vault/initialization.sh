#!/bin/bash

groupadd -g 985 vault
useradd -u 989 -g 985 vault
ln -s /usr/local/openiam/vault/vault /usr/bin/vault
vault -autocomplete-install
complete -C /usr/bin/vault vault
useradd --system --home $HOME_DIR/vault --shell /bin/false vault

if [ -f "/usr/local/openiam/logs/vault.out" ]; then
   rm /usr/local/openiam/logs/vault.out
fi

if [ ! -d "/usr/local/openiam/logs" ]; then
   mkdir /usr/local/openiam/logs
fi
touch /usr/local/openiam/logs/vault.out
chown vault:vault /usr/local/openiam/logs/vault.out
