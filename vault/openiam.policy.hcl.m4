storage "etcd" {
  address  = "http://NODE_1_IP:2379,http://NODE_2_IP:2379,http://NODE_3_IP:2379"
  etcd_api = "v3"
}

listener "tcp" {
  address = "0.0.0.0:8200"
  tls_min_version = "tls12"
  tls_cert_file = "/usr/local/openiam/vault/certs/vault.crt"
  tls_key_file  = "/usr/local/openiam/vault/certs/vault.key"
  tls_client_ca_file  = "/usr/local/openiam/vault/certs/vault.ca.key"
}
disable_mlock = true
api_addr="localhost:8200"
