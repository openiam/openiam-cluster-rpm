#!/bin/bash

set -e

. infra.conf


# check free disk space in /
FREE_SPACE=$(df --output=avail -k / |tail -n 1)
if [ "$FREE_SPACE" -lt "80000000" ]; then
  echo "Not enough free disk space on the node to install Openiam"
  echo "System requirements are: 80 GB - Non-Production, 200 GB - Production"
  exit
fi

# configure hosts file
if cat /etc/hosts | grep ${CLUSTER_IPS[1]}; then
  echo "hosts is alredy configured"
else
  echo "${CLUSTER_IPS[1]}  ${CLUSTER_NODES[1]}" >> /etc/hosts
  echo "${CLUSTER_IPS[2]}  ${CLUSTER_NODES[2]}" >> /etc/hosts
  echo "${CLUSTER_IPS[3]}  ${CLUSTER_NODES[3]}" >> /etc/hosts
  echo "${CLUSTER_IPS[1]}" "master" >> /etc/hosts

# propagate hosts file
cat << 'EOF' > ~/.ssh/config
Host *
    StrictHostKeyChecking no
EOF
chmod 400 ~/.ssh/config

  scp /etc/hosts ${CLUSTER_IPS[2]}:/etc/hosts
  scp /etc/hosts ${CLUSTER_IPS[3]}:/etc/hosts
fi

# Ppopagate ssh key
scp ~/.ssh/id_rsa ${CLUSTER_IPS[2]}:~/.ssh/
scp ~/.ssh/id_rsa ${CLUSTER_IPS[3]}:~/.ssh/

# create cluster util
if [ ! -f "/usr/bin/cluster" ]; then
cat << 'EOF' > /usr/bin/cluster
#!/bin/bash
command=$@
echo "Execute $command"
EOF

echo "echo 'on ${CLUSTER_NODES[1]}'" >> /usr/bin/cluster 
echo "ssh ${CLUSTER_IPS[1]} \$command" >> /usr/bin/cluster
echo "echo 'on ${CLUSTER_NODES[2]}'" >> /usr/bin/cluster 
echo "ssh ${CLUSTER_IPS[2]} \$command" >> /usr/bin/cluster
echo "echo 'on ${CLUSTER_NODES[3]}'" >> /usr/bin/cluster 
echo "ssh ${CLUSTER_IPS[3]} \$command" >> /usr/bin/cluster

chmod +x /usr/bin/cluster
fi

# Install packages on all cluster nodes
cluster dnf install wget -y
cluster dnf install socat -y
cluster dnf install httpd -y
cluster dnf install jq m4 -y
cluster dnf install net-tools -y

if [ ! "$(systemctl is-active firewalld)" == "active" ]; then
  cluster dnf install firewalld -y
  cluster systemctl start firewalld
fi

function configure_nfs() {
  # Configure NFS server
  cluster dnf install nfs-utils -y
  systemctl enable --now nfs-server rpcbind
  mkdir -p /opt/nfs_share/openiam/{upload,conf,vault/certs}
  echo "/opt/nfs_share   $NETWORK(rw,sync,no_root_squash,no_subtree_check)" > /etc/exports
  exportfs -rav
  firewall-cmd --add-service=nfs --permanent
  firewall-cmd --add-service={nfs3,mountd,rpc-bind} --permanent
  firewall-cmd --reload

  cluster mount -t nfs $NFS_SERVER:/opt/nfs_share/openiam/upload /usr/local/openiam/upload
  cluster mount -t nfs $NFS_SERVER:/opt/nfs_share/openiam/conf /usr/local/openiam/conf
  cluster mount -t nfs $NFS_SERVER:/opt/nfs_share/openiam/vault/certs /usr/local/openiam/vault/certs

  for i in 1 2 3; do
    ssh ${CLUSTER_IPS[$i]} echo "$NFS_SERVER:/opt/nfs_share/openiam/upload /usr/local/openiam/upload nfs defaults 0 0" \>\> /etc/fstab
    ssh ${CLUSTER_IPS[$i]} echo "$NFS_SERVER:/opt/nfs_share/openiam/conf /usr/local/openiam/conf nfs defaults 0 0" \>\> /etc/fstab
    ssh ${CLUSTER_IPS[$i]} echo "$NFS_SERVER:/opt/nfs_share/openiam/vault/certs /usr/local/openiam/vault/certs nfs defaults 0 0" \>\> /etc/fstab
  done
}


function configure_glusterfs() {
  cluster dnf install glusterfs -y
  cluster dnf install glusterfs-server -y
  cluster systemctl enable --now glusterfsd.service
  cluster systemctl enable --now glusterd
  cluster firewall-cmd --add-service=glusterfs --permanent
  cluster firewall-cmd --reload

  gluster peer probe ${CLUSTER_NODES[2]}
  gluster peer probe ${CLUSTER_NODES[3]}
  gluster peer status
  gluster pool list
  cluster mkdir -p /usr/local/openiam/gfsbricks/vault
  cluster mkdir -p /usr/local/openiam/gfsbricks/conf
  cp -rf /usr/local/openiam/conf /usr/local/openiam/back_conf/

  gluster volume create vault replica 3 transport tcp ${CLUSTER_NODES[1]}:/usr/local/openiam/gfsbricks/vault ${CLUSTER_NODES[2]}:/usr/local/openiam/gfsbricks/vault ${CLUSTER_NODES[3]}:/usr/local/openiam/gfsbricks/vault force
  gluster volume create conf replica 3 transport tcp ${CLUSTER_NODES[1]}:/usr/local/openiam/gfsbricks/conf ${CLUSTER_NODES[2]}:/usr/local/openiam/gfsbricks/conf ${CLUSTER_NODES[3]}:/usr/local/openiam/gfsbricks/conf force
  gluster volume start conf
  gluster volume start vault

  for i in 1 2 3; do
    ssh ${CLUSTER_IPS[$i]} echo "localhost:conf /usr/local/openiam/conf/ glusterfs defaults,_netdev 0 0" \>\> /etc/fstab
    ssh ${CLUSTER_IPS[$i]} echo "localhost:vault /usr/local/openiam/vault/certs/ glusterfs defaults,_netdev 0 0" \>\> /etc/fstab
  done

  cluster mount -a
  cp -rf /usr/local/openiam/back_conf/* /usr/local/openiam/conf/
  chown -R openiam:openiam /usr/local/openiam/conf
}


function configure_haproxy() {
  cluster dnf install haproxy -y
  cluster dnf install keepalived -y

  cluster sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
  
  m4 -DIP="${CLUSTER_IPS[1]}" -DPEER_1="${CLUSTER_IPS[2]}" -DPEER_2="${CLUSTER_IPS[3]}" -DVIRTUAL_ADDRESS="${KEEPALIVE_VIRTUAL_ADDRESS}" haproxy/keepalived.conf.m4 > haproxy/keepalived.conf.${CLUSTER_IPS[1]}
  m4 -DIP="${CLUSTER_IPS[2]}" -DPEER_1="${CLUSTER_IPS[1]}" -DPEER_2="${CLUSTER_IPS[3]}" -DVIRTUAL_ADDRESS="${KEEPALIVE_VIRTUAL_ADDRESS}" haproxy/keepalived.conf.m4 > haproxy/keepalived.conf.${CLUSTER_IPS[2]}
  m4 -DIP="${CLUSTER_IPS[3]}" -DPEER_1="${CLUSTER_IPS[1]}" -DPEER_2="${CLUSTER_IPS[2]}" -DVIRTUAL_ADDRESS="${KEEPALIVE_VIRTUAL_ADDRESS}" haproxy/keepalived.conf.m4 > haproxy/keepalived.conf.${CLUSTER_IPS[3]}

  #m4 -DNODE_1_IP="${CLUSTER_IPS[1]}" -DNODE_2_IP="${CLUSTER_IPS[2]}" -DNODE_3_IP="${CLUSTER_IPS[3]}" haproxy/conf.d/redis.cfg.m4 > haproxy/conf.d/redis.cfg
  m4 -DVAULT_PORT="${HAPROXY_VAULT_PORT}" -DNODE_1="${CLUSTER_IPS[1]}" -DNODE_2="${CLUSTER_IPS[2]}" -DNODE_3="${CLUSTER_IPS[3]}" haproxy/conf.d/vault.cfg.m4 > haproxy/conf.d/vault.cfg
  m4 -DJANUS_PORT="${HAPROXY_JANUS_PORT}" -DNODE_1="${CLUSTER_IPS[1]}" -DNODE_2="${CLUSTER_IPS[2]}" -DNODE_3="${CLUSTER_IPS[3]}" haproxy/conf.d/janusgraph.cfg.m4 > haproxy/conf.d/janusgraph.cfg
  m4 -DESB_PORT="${HAPROXY_ESB_PORT}" -DNODE_1="${CLUSTER_IPS[1]}" -DNODE_2="${CLUSTER_IPS[2]}" -DNODE_3="${CLUSTER_IPS[3]}" haproxy/conf.d/openiam-esb.cfg.m4 > haproxy/conf.d/openiam-esb.cfg
  m4 -DUI_PORT="${HAPROXY_UI_PORT}" -DNODE_1="${CLUSTER_IPS[1]}" -DNODE_2="${CLUSTER_IPS[2]}" -DNODE_3="${CLUSTER_IPS[3]}" haproxy/conf.d/openiam-ui.cfg.m4 > haproxy/conf.d/openiam-ui.cfg
  m4 -DES_PORT="${HAPROXY_ES_PORT}" -DNODE_1="${CLUSTER_IPS[1]}" -DNODE_2="${CLUSTER_IPS[2]}" -DNODE_3="${CLUSTER_IPS[3]}" haproxy/conf.d/elasticsearch.cfg.m4 > haproxy/conf.d/elasticsearch.cfg
  m4 -DRMQ_PORT="${HAPROXY_RABBIT_PORT}" -DNODE_1="${CLUSTER_IPS[1]}" -DNODE_2="${CLUSTER_IPS[2]}" -DNODE_3="${CLUSTER_IPS[3]}" haproxy/conf.d/rabbit.cfg.m4 > haproxy/conf.d/rabbit.cfg
  m4 -DRMQ_UI_PORT="${HAPROXY_RABBIT_UI_PORT}" -DNODE_1="${CLUSTER_IPS[1]}" -DNODE_2="${CLUSTER_IPS[2]}" -DNODE_3="${CLUSTER_IPS[3]}" haproxy/conf.d/rabbitmq-ui.cfg.m4 > haproxy/conf.d/rabbitmq-ui.cfg

  # Distribute keepalive configs among all nodes
  scp haproxy/keepalived.conf.${CLUSTER_IPS[1]} ${CLUSTER_IPS[1]}:/etc/keepalived/keepalived.conf 
  scp haproxy/keepalived.conf.${CLUSTER_IPS[2]} ${CLUSTER_IPS[2]}:/etc/keepalived/keepalived.conf 
  scp haproxy/keepalived.conf.${CLUSTER_IPS[3]} ${CLUSTER_IPS[3]}:/etc/keepalived/keepalived.conf 


# Distribute HAProxy configs among all nodes
  for i in 1 2 3; do
    #echo "Copy redis.cfg to $host..."
    #scp haproxy/conf.d/redis.cfg $host:/etc/haproxy/conf.d/redis.cfg 
    echo "Copy vault.cfg to "${CLUSTER_IPS[$i]}"..."
    scp haproxy/conf.d/vault.cfg ${CLUSTER_IPS[$i]}:/etc/haproxy/conf.d/vault.cfg 
    echo "Copy janusgraph.cfg to "${CLUSTER_IPS[$i]}"..."
    scp haproxy/conf.d/janusgraph.cfg ${CLUSTER_IPS[$i]}:/etc/haproxy/conf.d/janusgraph.cfg 
    echo "Copy openiam-esb.cfg to "${CLUSTER_IPS[$i]}"..."
    scp haproxy/conf.d/openiam-esb.cfg ${CLUSTER_IPS[$i]}:/etc/haproxy/conf.d/openiam-esb.cfg 
    echo "Copy openiam-ui.cfg to "${CLUSTER_IPS[$i]}"..."
    scp haproxy/conf.d/openiam-ui.cfg ${CLUSTER_IPS[$i]}:/etc/haproxy/conf.d/openiam-ui.cfg 
    echo "Copy elasticsearch.cfg to "${CLUSTER_IPS[$i]}"..."
    scp haproxy/conf.d/elasticsearch.cfg ${CLUSTER_IPS[$i]}:/etc/haproxy/conf.d/elasticsearch.cfg 
    echo "Copy rabbit.cfg to "${CLUSTER_IPS[$i]}"..."
    scp haproxy/conf.d/rabbit.cfg ${CLUSTER_IPS[$i]}:/etc/haproxy/conf.d/rabbit.cfg 
    echo "Copy rabbitmq-ui.cfg to "${CLUSTER_IPS[$i]}"..."
    scp haproxy/conf.d/rabbitmq-ui.cfg ${CLUSTER_IPS[$i]}:/etc/haproxy/conf.d/rabbitmq-ui.cfg 
  done

  cluster systemctl enable keepalived
  cluster systemctl start keepalived
  cluster systemctl enable haproxy
  cluster systemctl start haproxy
}

function prompt_db() {
  read -p "Would you like to install MariaDB Galera Cluster? [y/n]:" is_mariadb
  case "$is_mariadb" in
    [yY][eE][sS]|[yY])
        configure_galera
        ;;
    [nN][oO]|[nN])
        echo "Skip MariaDB Galera installation"
        ;;
    *)
      	echo "Please answer [y/n]"
        prompt_db
        ;;
  esac
}

function configure_galera() {
  cluster dnf install mariadb-server-galera -y
  ## Configure sysctl
  for i in 1 2 3; do
    ssh ${CLUSTER_IPS[$i]} echo "vm.max_map_count = 262144" \>\> /etc/sysctl.conf
    ssh ${CLUSTER_IPS[$i]} echo "net.ipv6.conf.all.disable_ipv6 = 1" \>\> /etc/sysctl.conf
    ssh ${CLUSTER_IPS[$i]} echo "net.ipv6.conf.default.disable_ipv6 = 1" \>\> /etc/sysctl.conf
    ssh ${CLUSTER_IPS[$i]} echo "net.ipv4.ip_nonlocal_bind = 1" \> /etc/sysctl.d/99-nonlocal-bind.conf
  done

  cluster sysctl -p

  ## Galera RDBMS Cluster configuration
  cluster sed -i 's/my_wsrep_cluster/OpenIAM_Cluster/g' /etc/my.cnf.d/galera.cnf
  for i in 1 2 3; do
    ssh ${CLUSTER_IPS[$i]} echo "wsrep_cluster_address=${GALERA_CLUSTER_ADDRESS}" \>\> /etc/my.cnf.d/galera.cnf
    ssh ${CLUSTER_IPS[$i]} echo "wsrep_node_address="${CLUSTER_IPS[$i]}"" \>\> /etc/my.cnf.d/galera.cnf
  done

  galera_new_cluster
  sleep 10
  cluster systemctl enable --now mariadb

  mysql_secure_installation
}


# Install openiam
if [ ! -f "/usr/src/openiam-4.2.X.noarch.x86_64.rpm" ]; then
  cluster curl https://download.openiam.com/release/enterprise/$OPENIAM_VERSION/rpm/openiam-$OPENIAM_VERSION.noarch.x86_64.rpm --output /usr/src/openiam-4.2.X.noarch.x86_64.rpm
fi

for i in 1 2 3; do
  ssh ${CLUSTER_IPS[$i]} yum install /usr/src/openiam-4.2.X.noarch.x86_64.rpm -y
  ssh ${CLUSTER_IPS[$i]} shutdown -c
done

mv /usr/local/openiam/conf /tmp/

cluster mkdir -p /usr/local/openiam/{upload,conf,vault/certs}

if [ "$FS" == "glusterfs" ]; then
   configure_glusterfs
fi
if [ "$FS" == "nfs" ]; then
   configure_nfs
fi

## Configure firewall for infra
cluster setenforce 0
cluster firewall-cmd --add-port={4567/tcp,4568/tcp,4444/tcp} --permanent
cluster firewall-cmd --add-port={3306/tcp,4567/tcp,4568/tcp,4444/tcp} --permanent
cluster firewall-cmd --add-port={2379/tcp,2380/tcp,9200/tcp,9300/tcp} --permanent
cluster firewall-cmd --add-port={15672/tcp,25672/tcp,5671-5672/tcp,35672-35682/tcp,4369/tcp} --permanent
cluster firewall-cmd --add-port={6379/tcp,26379/tcp} --permanent
cluster firewall-cmd --add-port={7000/tcp,7001/tcp} --permanent
cluster firewall-cmd --add-port={8000/tcp,8001/tcp} --permanent
cluster firewall-cmd --add-port={8182/tcp,${HAPROXY_JANUS_PORT}/tcp} --permanent
cluster firewall-cmd --add-port={8200/tcp,${HAPROXY_VAULT_PORT}/tcp} --permanent
cluster firewall-cmd --add-port=${HAPROXY_UI_PORT}/tcp --permanent
cluster firewall-cmd --add-port=${HAPROXY_ESB_PORT}/tcp --permanent
cluster firewall-cmd --add-port=${HAPROXY_ES_PORT}/tcp --permanent
cluster firewall-cmd --add-port=${HAPROXY_RABBIT_PORT}/tcp --permanent
cluster firewall-cmd --add-port=${HAPROXY_RABBIT_UI_PORT}/tcp --permanent
cluster firewall-cmd --reload

. /usr/local/openiam/env.conf

RH_VERSION=$(cat /etc/os-release | grep PLATFORM | awk -F: '{print substr($2,1,3)}')
OIAM_VERSION=$(cat /usr/local/openiam/version)


dnf install rsync -y
rsync -a /tmp/conf/ /usr/local/openiam/conf

if [ ! -d "$HOME_DIR/conf/properties" ]; then
  mkdir $HOME_DIR/conf/properties
fi

cluster curl  https://download.openiam.com/release/enterprise/${OIAM_VERSION}/dependencies/${RH_VERSION}/openiamrepo.tar.gz --output ${HOME_DIR}/openiamrepo.tar.gz
cluster tar -xvf ${HOME_DIR}/openiamrepo.tar.gz --directory=${HOME_DIR}/

cluster curl  https://download.openiam.com/release/enterprise/${OIAM_VERSION}//binaries/backend.tar.gz --output ${HOME_DIR}/backend.tar.gz
cluster curl  https://download.openiam.com/release/enterprise/${OIAM_VERSION}//binaries/frontend.tar.gz --output ${HOME_DIR}/frontend.tar.gz

cluster mkdir -p "${HOME_DIR}/services/bin/"
cluster mkdir -p "${HOME_DIR}/logs/"
cluster mkdir -p "${HOME_DIR}/sas_lib/"
cluster tar -xvf ${HOME_DIR}/backend.tar.gz --directory=${HOME_DIR}/services/bin/
cluster tar -xvf ${HOME_DIR}/frontend.tar.gz --directory=${HOME_DIR}/ui/webapps/
cluster chown -R openiam:openiam ${HOME_DIR}/services/bin/
cluster chown -R openiam:openiam ${HOME_DIR}/ui/webapps/
cluster mv ${HOME_DIR}/services/bin/sas-lib.zip ${HOME_DIR}/sas_lib/

for i in 1 2 3; do
  scp ${HOME_DIR}/OpenIAM-Base-Local.repo ${CLUSTER_IPS[$i]}:/etc/yum.repos.d/
done

cluster yum update --skip-broken --nobest -y

### HA Proxy cluster
configure_haproxy

### Galera MariaDB cluster
prompt_db

### etcd
cd etcd
./init.sh
cd ..

### Vault
cd vault
./init.sh
cd ..

### Elasticsearch
cd elasticsearch
./init.sh
cd ..

### RabbitMQ
cd rabbitmq
./init.sh
cd ..

### Redis
cd redis
./init.sh
cd ..

### Migrations
cd database
./init.sh
cd ..

### Janusgraph
cd graph
./init.sh
cd ..

### Curator
$HOME_DIR/utils/curator/init.sh

### Proxy
cd proxy
./init.sh
cd ..

### Openiam
cd services
./init.sh
cd ..
