#!/bin/bash

. ../infra.conf

echo "Install ETCD"

cluster rpm -i /usr/local/openiam/openiamrepo/etcd-3.2.21-2.el8.x86_64.rpm

m4 -DNODE_NAME="${CLUSTER_NODES[1]}" -DNODE_IP="${CLUSTER_IPS[1]}" -DNODE_1_NAME="${CLUSTER_NODES[1]}" -DNODE_1_IP="${CLUSTER_IPS[1]}" -DNODE_2_NAME="${CLUSTER_NODES[2]}" -DNODE_2_IP="${CLUSTER_IPS[2]}" -DNODE_3_NAME="${CLUSTER_NODES[3]}" -DNODE_3_IP="${CLUSTER_IPS[3]}" ./etcd.conf.m4 > ./${CLUSTER_NODES[1]}_etcd.conf
m4 -DNODE_NAME="${CLUSTER_NODES[2]}" -DNODE_IP="${CLUSTER_IPS[2]}" -DNODE_1_NAME="${CLUSTER_NODES[1]}" -DNODE_1_IP="${CLUSTER_IPS[1]}" -DNODE_2_NAME="${CLUSTER_NODES[2]}" -DNODE_2_IP="${CLUSTER_IPS[2]}" -DNODE_3_NAME="${CLUSTER_NODES[3]}" -DNODE_3_IP="${CLUSTER_IPS[3]}" ./etcd.conf.m4 > ./${CLUSTER_NODES[2]}_etcd.conf
m4 -DNODE_NAME="${CLUSTER_NODES[3]}" -DNODE_IP="${CLUSTER_IPS[3]}" -DNODE_1_NAME="${CLUSTER_NODES[1]}" -DNODE_1_IP="${CLUSTER_IPS[1]}" -DNODE_2_NAME="${CLUSTER_NODES[2]}" -DNODE_2_IP="${CLUSTER_IPS[2]}" -DNODE_3_NAME="${CLUSTER_NODES[3]}" -DNODE_3_IP="${CLUSTER_IPS[3]}" ./etcd.conf.m4 > ./${CLUSTER_NODES[3]}_etcd.conf

for i  in 1 2 3; do
  scp ${CLUSTER_NODES[$i]}_etcd.conf ${CLUSTER_IPS[$i]}:/etc/etcd/etcd.conf
done
cluster systemctl enable etcd

echo "Start etcd on ${CLUSTER_NODES[1]}"
systemctl start etcd&
echo "Start etcd on ${CLUSTER_NODES[2]}"
ssh ${CLUSTER_NODES[2]} systemctl start etcd&
echo "Start etcd on ${CLUSTER_NODES[3]}"
ssh ${CLUSTER_NODES[3]} systemctl start etcd& 

echo "Waiting for etcd cluster is ready..."
sleep 60;
#echo "Check That it's started"
#etcdctl member list


